{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dask@NERSC: A Gentle Introduction\n",
    "\n",
    "This notebook shows you how to use [Dask Distributed](https://distributed.dask.org/en/stable/) to do parallel computations using the [NERSC Jupyter service.](https://docs.nersc.gov/services/jupyter/)\n",
    "As we streamline the Dask experience at NERSC this notebook will be updated and simplified.\n",
    "\n",
    "All this notebook does is use a crude Monte Carlo calculation to estimate the value of $\\pi$.\n",
    "We use the dart-board method:\n",
    "\n",
    "- Take the first quadrant of the unit square, and within that the first quadrant of the unit circle.\n",
    "- Simulate throwing darts randomly at the square with a uniformly random distribution in x and y.\n",
    "- Take the ratio of darts landing within the first quadrant of the unit circle to all darts thrown.\n",
    "- Multiply the result by 4 to approximage $\\pi$.\n",
    "- The more darts thrown, the more precise the result."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create a Conda Environment Kernel for Dask\n",
    "\n",
    "Here are some simple instructions for building a Jupyter kernel from a Conda environment to run Dask at NERSC.\n",
    "This one uses `dask-mpi` but you don't have to do that.\n",
    "We'll have a notebook tutorial for `dask-jobqueue` soon!\n",
    "\n",
    "There are some important details here!\n",
    "(See the note at the end of this section.)\n",
    "At the command line, either SSHed into Cori or using a Jupyter terminal, do:\n",
    "\n",
    "    module load python\n",
    "    conda create -c conda-forge -n nersc-dask \\\n",
    "        python=3 dask distributed ipykernel numpy\n",
    "        \n",
    "This will build you the *start* of a Conda environment for Dask.\n",
    "We use the latest builds from the \"conda-forge\" channel.\n",
    "Now, activate the created environment:\n",
    "\n",
    "    source activate nersc-dask\n",
    "    \n",
    "The next step builds `mpi4py` against Cray MPICH.\n",
    "It's the usual steps documented [here.](https://docs.nersc.gov/programming/high-level-environments/python/mpi4py/#mpi4py-in-your-custom-conda-environment)\n",
    "\n",
    "    wget https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-3.0.0.tar.gz\n",
    "    tar zxvf mpi4py-3.0.0.tar.gz\n",
    "    cd mpi4py-3.0.0\n",
    "    module swap PrgEnv-intel PrgEnv-gnu\n",
    "    module unload craype-hugepages2M\n",
    "    python setup.py build --mpicc=\"$(which cc) -shared\"\n",
    "    python setup.py install\n",
    "\n",
    "The final step installs `dask-mpi` itself.\n",
    "\n",
    "    conda install -c conda-forge --no-deps dask-mpi\n",
    "    \n",
    "Because we have `nb_conda_kernels` installed on NERSC's JupyterHub, the conda env you just built should show up as a Jupyter kernel in your notebook.\n",
    "It may be called \"Python [conda env:.conda-nersc-dask]\".\n",
    "You can use it with this notebook if you've built it as above.\n",
    "\n",
    "### Comment\n",
    "\n",
    "The above set of steps is more cumbersome than we would like.\n",
    "In the future we will have the Python module set up to provide the required packages \"out-of-the-box\" for you.\n",
    "It's just that at this point in time there are some issues setting that up.\n",
    "So: Please check back was we make progress, this notebook should become simpler over time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Start Up a Dask Cluster on Compute Nodes\n",
    "\n",
    "Now we need to start up a Dask cluster.\n",
    "This will consist of a *scheduler*, *workers* and a *dashboard*.\n",
    "There are a few ways to start up Dask clusters.\n",
    "Here we demonstrate `dask-mpi`.\n",
    "\n",
    "**Open up a Jupyter terminal pane, `cd $SCRATCH`, and run the following:**\n",
    "\n",
    "    module load python\n",
    "    source activate nersc-dask\n",
    "    \n",
    "    export OMP_NUM_THREADS=1\n",
    "    salloc \\\n",
    "        -N 2 \\\n",
    "        -n 64 \\\n",
    "        -c 2 \\\n",
    "        -t 20 \\\n",
    "        -C haswell \\\n",
    "        -q interactive \\\n",
    "        srun -u python -u \\\n",
    "            $(which dask-mpi) \\\n",
    "                --scheduler-file=$SCRATCH/scheduler.json \\\n",
    "                --dashboard-address=0 \\\n",
    "                --nthreads=1 \\\n",
    "                --memory-limit=0 \\\n",
    "                --no-nanny \\\n",
    "                --local-directory=/tmp\n",
    "                \n",
    "This submits a job to the Interactive QOS.\n",
    "The `-u` options are included to prevent buffering, which can he helpful in debugging but isn't strictly necessary.\n",
    "For more information about the `dask-mpi` options, see [this page](http://mpi.dask.org/en/latest/).\n",
    "\n",
    "Again, this method of getting resources for your Dask job is pretty clunky and cumbersome.\n",
    "We're working on getting the setup integrated into the Dask itself."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import Dask and Customize Configuration\n",
    "\n",
    "Dask will use a JSON file shared between all the nodes to coordinate connections.\n",
    "We also set up a custom configuration for enabling you to connect to the Dask dashboard from Jupyter.\n",
    "This custom config can be done in your Dask Distrubited configuration if you like.\n",
    "\n",
    "**You can execute this cell before the job starts, but wait for the job before proceeding past it.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import dask\n",
    "from dask.distributed import Client\n",
    " \n",
    "scheduler_file = os.path.join(os.environ[\"SCRATCH\"], \"scheduler.json\")\n",
    "dask.config.config[\"distributed\"][\"dashboard\"][\"link\"] = \"{JUPYTERHUB_SERVICE_PREFIX}proxy/{host}:{port}/status\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Connect to the Scheduler\n",
    "\n",
    "Once the job has started, connect to the scheduler.\n",
    "You can run this step before the job starts but it will just block your notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "client = Client(scheduler_file=scheduler_file)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set Us Up the Dashboard\n",
    "\n",
    "In the output above you should see a little widget describing the Dask cluster.\n",
    "Click the link for the dashboard.\n",
    "It will open another tab for you with the Dask cluster dashboard interface.\n",
    "You can take that full URL and paste it into the Dask dashboard widget on the left side of the JupyterLab page.\n",
    "(See thge Dask logo on the left.)\n",
    "Open up the Task Stream, Progress, and Memory Use panes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Define the Simulation Function\n",
    "\n",
    "The method we are using (Monte Carlo) parallelizes trivially.\n",
    "You can combine the results of multiple random trials to get a more precise answer.\n",
    "Here's the function that implements the dart board simulation.\n",
    "Nothing fancy here, it just returns the number of hits inside the circle, and thet total number of throws (`count`).\n",
    "Each run should get a unique seed to try to help ensure all the trials are \"independent.\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "def simulate(seed, count=100):\n",
    "    np.random.seed(seed)\n",
    "    xy = np.random.uniform(size=(count, 2))\n",
    "    return ((xy * xy).sum(1) < 1.0).sum(), count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Use the `map()` Function to Run Distribute Tasks to Workers\n",
    "\n",
    "We're going to ask for some very large number of total throws and chop them up into a smaller number of tasks.\n",
    "Our Dask cluster has a scheduler that figures out how to distribute all the work.\n",
    "Typical scheduling overhead is 1 millisecond per task.\n",
    "When you start the calculation it may thus take a little time for the dashboard to react."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "total = 100000000000\n",
    "tasks = 10000\n",
    "count = total // tasks\n",
    "futures = client.map(simulate, list(9876543 + np.arange(tasks, dtype=int)), count=count)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Look into the Future(s)\n",
    "\n",
    "The `map()` function and a lot of other functions from Dask return \"futures\" which are placeholders for computation.\n",
    "These may be not done yet, or they may be done, and you need to do something to realize them into their final result form.\n",
    "Let's submit a final reducer task that computes the sum of hits and divides it by the sum of all simulated throws.\n",
    "Here's the reducer function, with a multiplication by 4 to get to our estimate of $\\pi$.\n",
    "It takes in the futures and acts on them like they're real results.\n",
    "That's the magic of Dask."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def reduce(results):\n",
    "    total_hits = 0\n",
    "    total_count = 0\n",
    "    for hits, count in results:\n",
    "        total_hits += hits\n",
    "        total_count += count\n",
    "    return 4.0 * total_hits / total_count"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And finally we submit the reducer, which we can do even before all the simulations are done:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "client.submit(reduce, futures).result()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Further Reading\n",
    "\n",
    "This notebook just touches the surface of the things you can do with Dask at NERSC.\n",
    "\n",
    "- [Dask](https://docs.dask.org/en/stable/)\n",
    "- [Dask Distributed](https://distributed.dask.org/en/stable/)\n",
    "- [Dask-MPI](http://mpi.dask.org/en/latest/)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:.conda-nersc-dask]",
   "language": "python",
   "name": "conda-env-.conda-nersc-dask-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.0"
  },
  "toc-autonumbering": false,
  "toc-showtags": false
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
